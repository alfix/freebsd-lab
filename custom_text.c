// custom text <= 0.3
void custom_text(char *text, char *buf)
{
	int i, j;

	i = j = 0;
	while (text[i] != '\0') {
		switch (text[i]) {
		case '\\':
			buf[j] = '\\';
			switch (text[i+1]) {
			case '\\':
				i++;
				break;
			case 'n':
				if (no_nl_expand_opt) {
					j++;
					buf[j] = 'n';
				} else
					buf[j] = '\n';
				i++;
				break;
			case 't':
				if (no_collapse_opt) {
					j++;
					buf[j] = 't';
				} else
					buf[j] = '\t';
				i++;
				break;
			}
			break;
		case '\n':
			buf[j] = cr_wrap_opt ? ' ' : '\n';
			break;
		case '\t':
			buf[j] = no_collapse_opt ? '\t' : ' ';
			break;
		default:
			buf[j] = text[i];
		}
		i++;
		j += (buf[j] == ' ' && trim_opt && j > 0 && buf[j-1] == ' ') ?
		    0 : 1;
	}
	buf[j] = '\0';
}
