#include <curses.h>
#include <unistd.h>

/* cc cursortest.c -o cursortest -lncurses ; ./cursortest */

int main()
{
	int i, j, c, error;


	if (initscr() == NULL)
		printf("Cannot init curses (initscr)\n");

	error = OK;
	error += keypad(stdscr, TRUE);
	nl();
	error += cbreak();
	error += noecho();
	if (error != OK) {
		endwin();
		printf("Cannot init curses (keypad and cursor)\n");
	}

	c = 1;
	error += start_color();
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			error += init_pair(c, i, j);
			c++;
		}
	}

	move(0, 1);
	if (error == OK && has_colors())
		mvaddstr(0, 0, "terminal supports colors\n");
	else
		mvaddstr(0, 0, "terminal is black/white\n");
	refresh();

	move (1,0);
	addstr("Supported cursors: ");
	if (curs_set(0) != ERR)
		addstr("curs_set(0) ");
	if (curs_set(1) != ERR)
		addstr("curs_set(1) ");
	if (curs_set(2) != ERR)
		addstr("curs_set(2)");

	/* cursor test */
	curs_set(0);
	mvaddstr(3,1, "ooXoo  <-- if visible the cursor will be on the X (now hidden).");
	mvaddstr(4,1, "           <Press a key to start!>");
	move(3,3); refresh();
	getch();

	curs_set(1);
	mvaddstr(6,1, "ooXoo  <-- cursor (1) on X, <Press a Key!>");
	move(6,3); refresh();
	getch();

	curs_set(2);
	mvaddstr(8,1, "ooXoo  <-- cursor (2) on X, <Press a Key!>");
	move(8,3); refresh();
	getch();

	/* Colors */
	attron(COLOR_PAIR(34));
	curs_set(1);
	mvaddstr(10,1, "ooXoo  <-- cursor (1) on X, <Press a Key!>");
	move(10,3); refresh();
	getch();

	curs_set(2);
	mvaddstr(12,1, "ooXoo  <-- cursor (2) on X, <Press a Key!>");
	move(12,3); refresh();
	getch();
	attroff(COLOR_PAIR(34));

	/* Manual cursor */
	curs_set(0);
#define escape(str) str,sizeof(str)
	write(1, escape("\x1b[?25h"));
	mvaddstr(14,1, "ooXoo  <-- cursor Manual on X, <Press a Key!>");
	move(14,3); refresh();
	getch();
	write(1, escape("\x1b[?25l"));

	/* end */

	mvaddstr(18, 8, "Thank you to test! <Press a Key to close>");	
	getch();
	endwin();

	return (0);
}
