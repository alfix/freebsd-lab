#include <curses.h>
#include <locale.h>
#include <err.h>
#include <stdlib.h>

/*
 * % cc -D_XOPEN_SOURCE_EXTENDED ncurses_unicode_test.c -o ncurses_unicode_test -lncursesw
 * % ./ncurses_unicode_test
*/

int main()
{
	int error, u8;
	char *locale;

	if ((locale = setlocale(LC_ALL, "")) == NULL)
		err(1, "Cannot set locale");

	/* init ncurses */
	if (initscr() == NULL)
		errx(1, "Cannot init curses (initscr)\n");

	/* testing */
	box(stdscr, 0, 0);
	mvaddstr(1, 2, "Testing unicode ncurses.");

	mvaddstr(3, 3, "locale and ncurses.");
	mvprintw(4, 4, "1) locale: %s.", locale);
	mvprintw(5, 4, "2) ncurses version: %s, %d.", NCURSES_VERSION, NCURSES_VERSION_PATCH);
	u8 = tigetnum("U8");
	mvprintw(6, 4, "3) U8 cap: %d, TERM: %s", u8, getenv("TERM"));

	mvaddstr(9, 3, "Unicode chars.");
	mvaddstr(10, 4, "1) locale encode: あいうえお");
	mvaddwstr(11, 4, L"2) wchar: あいうえお");
	mvaddstr(12, 4, "3) Unicode border chars (ACS_*): ");
	addch(ACS_VLINE); addch(' ');
	addch(ACS_HLINE); addch(' ');
	addch(ACS_ULCORNER); addch(' ');
	addch(ACS_URCORNER); addch(' ');
	addch(ACS_LRCORNER); addch(' ');
	addch(ACS_LLCORNER); addch(' ');
	addch(ACS_RTEE); addch(' ');
	addch(ACS_LTEE); addch(' ');
	
	/* refresh and exit */
	mvaddstr(14, 2, "Thank you to test <Press a Key to close> ");
	refresh();
	getch();
	endwin();

	return (0);
}
