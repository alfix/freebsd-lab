./bsddialog --hline "Press arrows, TAB or ENTER" \
	--item-prefix --menu \
	"The system console driver for $OSNAME defaults to standard \"US\" keyboard map. Other keymaps can be chosen below." \
	0 0 0 \
	">>>" Continue "with default keymap" \
	""    Test     "default keymap" \
	"( )" Armenian "phonetic keymap" \
	"( )" Belarus "Codepage 1131" \
	"( )" Belarus "Codepage 1251"

