bsddialog --title "Network Selection" \
--ok-label Select --extra-button --extra-label Rescan \
--cancel-label Manual --help-button --help-label Finish \
--menu "\nSelect a wireless network to connect to." 0 0 0 \
Network1 "[WEP]" Network2 "[WPA]" Network3 "[ESS]"
