#include <curses.h>
#include <locale.h>
#include <err.h>
#include <stdlib.h>

/*
 * % cc -D_XOPEN_SOURCE_EXTENDED ncurses-acs-test.c -o ncurses-acs-test -lncursesw
 * % ./ncurses-acs-test
*/

int main()
{
	int u8;
	char *locale;

	if ((locale = setlocale(LC_ALL, "")) == NULL)
		err(1, "Cannot set locale");
	/* init ncurses */
	if (initscr() == NULL)
		errx(1, "Cannot init curses (initscr)\n");

	/* testing */
	box_set(stdscr, 0,0); /* uses WACS_  (wide chars) */
	//box(stdscr, 0, 0);  /* uses ACS_  (UTF8 chars) */
	mvaddstr(1, 2, "Testing Line-drawing");

	mvaddstr(3, 3, "locale and ncurses.");
	mvprintw(4, 4, "1) locale: %s.", locale);
	mvprintw(5, 4, "2) ncurses version: %s, %d.", NCURSES_VERSION, NCURSES_VERSION_PATCH);
	u8 = tigetnum("U8");
	mvprintw(6, 4, "3) U8 cap: %d, TERM: %s", u8, getenv("TERM"));

	mvaddstr(9, 3, "Unicode chars.");
	mvaddstr(10, 4, "1) locale encode: あいうえお");
	mvaddstr(11, 4, "2) Unicode chars border (ACS_*): ");
	addch(ACS_VLINE); addch(' ');
	addch(ACS_HLINE); addch(' ');
	addch(ACS_ULCORNER); addch(' ');
	addch(ACS_URCORNER); addch(' ');
	addch(ACS_LRCORNER); addch(' ');
	addch(ACS_LLCORNER); addch(' ');
	addch(ACS_RTEE); addch(' ');
	addch(ACS_LTEE); addch(' ');

	mvaddstr(13, 3, "Wide chars.");
	mvaddwstr(14, 4, L"1) wchar: あいうえお");
	mvaddstr(15, 4, "2) Wide chars border (WACS_*): ");
	add_wch(WACS_VLINE); addch(' ');
	add_wch(WACS_HLINE); addch(' ');
	add_wch(WACS_ULCORNER); addch(' ');
	add_wch(WACS_URCORNER); addch(' ');
	add_wch(WACS_LRCORNER); addch(' ');
	add_wch(WACS_LLCORNER); addch(' ');
	add_wch(WACS_RTEE); addch(' ');
	add_wch(WACS_LTEE); addch(' ');

	/* refresh and exit */
	mvaddstr(20, 2, "Thank you to test <Press a Key to close> ");
	refresh();
	getch();
	endwin();

	return (0);
}
