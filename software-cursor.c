static void
drawitemX(struct privateform *form, struct privateitem *item, bool focus)
{
	int color;
	unsigned int n, cols;
	wchar_t cursorch;

	/* Label */
	wattron(form->pad, t.dialog.color);
	mvwaddstr(form->pad, item->ylabel, item->xlabel, item->label);
	wattroff(form->pad, t.dialog.color);

	/* Field */
	if (item->readonly)
		color = t.form.readonlycolor;
	else if (item->fieldnocolor)
		color = t.dialog.color;
	else
		color = focus ? t.form.f_fieldcolor : t.form.fieldcolor;
	wattron(form->pad, color);
	mvwhline(form->pad, item->yfield, item->xfield, ' ', item->fieldcols);
	n = 0;
	cols = wcwidth(item->pubwbuf[item->xposdraw]);
	while (cols <= item->fieldcols && item->xposdraw + n <
	    wcslen(item->pubwbuf)) {
		n++;
		cols += wcwidth(item->pubwbuf[item->xposdraw + n]);

	}
	mvwaddnwstr(form->pad, item->yfield, item->xfield,
	    &item->pubwbuf[item->xposdraw], n);
	wattroff(form->pad, color);

	/* cursor (with or without) */
	cursorch = item->pubwbuf[item->xposdraw + item->xcursor];
	if (cursorch == L'\0')
		cursorch = L' ';
	color = (focus && item->cursor) ? color | A_REVERSE : color;
	wattron(form->pad, color);
	mvwaddwch(form->pad, item->yfield, item->xfield + item->xcursor, cursorch);
	wattroff(form->pad, color);

	prefresh(form->pad, form->y, 0, form->ys, form->xs, form->ye, form->xe);

	/* Bottom Desc */
	move(SCREENLINES - 1, 2);
	clrtoeol();
	if (item->bottomdesc != NULL && focus) {
		attron(t.form.bottomdesccolor);
		addstr(item->bottomdesc);
		attroff(t.form.bottomdesccolor);
		refresh();
	}

	//curs_set((focus && item->cursor) ? 1 : 0 );
	//wmove(form->pad, item->yfield, item->xfield + item->xcursor);
	//prefresh(form->pad, form->y, 0, form->ys, form->xs, form->ye, form->xe);
}
